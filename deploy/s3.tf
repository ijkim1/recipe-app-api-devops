resource "aws_s3_bucket" "app_public_files" {
  bucket        = "${local.prefix}-internet2-files"
  acl           = "public-read"
  force_destroy = true
}
