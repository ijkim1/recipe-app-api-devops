variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "ij@internet2.edu"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "tsg-sample-app-key"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "515261047061.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "515261047061.dkr.ecr.us-east-1.amazonaws.com/nginx-proxy-sample:latest"
}

variable "django_secret_key" {
  description = "Secret key for the DJanog app"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "tsgdev.internet2.edu"
}

variable "subdomain" {
  description = "Subdomain per environments"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}
